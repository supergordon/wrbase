#ifndef _HACKSHIELD_
#define _HACKSHIELD_

typedef int (__cdecl* HSD3DScan)(DWORD param1, DWORD param2, DWORD param3, DWORD param4);
typedef int (__stdcall* HSSELFScan)(DWORD param1, DWORD param2, DWORD param3, DWORD param4);
typedef int (__stdcall* HSMEMScan)(DWORD param1, DWORD param2, DWORD param3);
typedef int (__cdecl* HSD3DVMTScan)(int param1);

class CHackshield
{
	public:
		void Hook();
		void Rehook();
		void RestoreHooks();
		void TurnOnOff(bool toggle);
		DWORD dwHSCounter;
		bool scanning;

		DWORD dwVMTScan;
		DWORD dwD3DScan;
		DWORD dwMEMScan;
		DWORD dwSELFScan;


		BYTE bVMTScan[6];
		BYTE bMEMScan[6];
		BYTE bSELFScan[6];
		BYTE bD3DScan[6];


		DWORD dwReset;
		DWORD dwEndScene;
		DWORD dwSetTransform;
		DWORD dwDrawIndexedPrimitive;
		DWORD dwSetStreamSource;

		BYTE bReset[5];
		BYTE bEndScene[5];
		BYTE bSetTransform[5];
		BYTE bDrawIndexedPrimitive[5];
		BYTE bSetStreamSource[5];

}; extern CHackshield *pHackshield;

#endif