#ifndef _SDK_
#define _SDK_

#include <windows.h>
#include <fstream>

#include "D3D/d3d8.h"
#include "D3D/d3dx8.h"

#include "Hackshield.h"
#include "Tools.h"
#include "DirectX.h"
#include "Menu.h"
#include "Address.h"

#pragma comment(lib, "D3D/d3d8.lib")
#pragma comment(lib, "D3D/d3dx8.lib")

#pragma warning(disable: 4311 4312 4244)

struct vector
{
	vector(float X=0, float Y=0, float Z=0) { x = X; y = Y; z = Z; }

	bool operator ==(vector &v) { return (x==v.x && y==v.y && z==v.z); }
	bool operator !=(vector &v) { return !(*this == v); }
	vector &operator +(float v) { x+=v; y+=v; z+=v; return *this; };
	vector &operator +(vector &v) { x+=v.x; y+=v.y; z+=v.z; return *this; };
	vector &operator -(float v) { x-=v; y-=v; z-=v; return *this; };
	vector &operator -(vector &v) { x-=v.x; y-=v.y; z-=v.z; return *this; };
	vector &operator *(float v) { x*=v; y*=v; z*=v; return *this; };
	vector &operator *(vector &v) { x*=v.x; y*=v.y; z*=v.z; return *this; };
	vector &operator /(float v) { x/=v; y/=v; z/=v; return *this; };
	vector &operator /(vector &v) { x/=v.x; y/=v.y; z/=v.z; return *this; };
	vector &operator =(vector &v) { x = v.x; y = v.y; z = v.z; return *this; }
	vector &operator =(float v) { x = v; y = v; z = v; return *this; }
	vector &operator -() { x = -x; y = -y; z = -z; return *this; }
	vector &operator +=(vector &v) { x += v.x; y += v.y; z+= v.z; return *this; };
	vector &operator +=(float v) { x += v; y += v; z+= v; return *this; };
	vector &operator -=(vector &v) { x -= v.x; y -= v.y; z-= v.z; return *this; };
	vector &operator -=(float v) { x -= v; y -= v; z-= v; return *this; };
	vector &operator *=(vector &v) { x *= v.x; y *= v.y; z*= v.z; return *this; };
	vector &operator *=(float v) { x *= v; y *= v; z*= v; return *this; };
	vector &operator /=(vector &v) { x /= v.x; y /= v.y; z/= v.z; return *this; };
	vector &operator /=(float v) { x /= v; y /= v; z/= v; return *this; };
	float Mag2D() { return sqrtf(x*x + y*y); }
	float Mag() { return sqrtf(x*x + y*y + z*z); }
	float DotProduct(vector v) { return x*v.x + y*v.y + z*v.z; }
	vector GetUnit() { return (*this) / Mag(); }
	void Normalize() { (*this) /= Mag(); }
	void SetMagnitude(float v) { (*this) *= v / Mag(); }

	float x,y,z;
};

struct CCVARS
{
	int D3D;
	int Wallhack;
	int Chams;
	int Fullbright;
	int Nofog;
	int Crosshair;

	int Aimbot;
	int Aimtoggle;
	int Aimkey;
	int Aimheight;
	int Aimfov;

	int ESP;
	int Name;
	int Distance;
	int Health;
	int IP;
	int State;
	int Weapon;

	int Player1;
	int Stamina;
	int Nofall;
	int Superjump;
	int Nobounds;
	int Removals;
	int Fly;
	int Movement;
	int Premium;
	int NoSpawn;
	int Telekill;

	int Player2;
	int Antikick;
	int Invisible;
	int OPK;
	int UnlAmmo;
	int NoDelay;
	int Headshot;
	int WalkThru;
	int ShootThru;
	int PremiumCrosshair;

	int Vehicle;
	int NoVehicleDamage;
	int VehicleJump;
	int VehicleInvisible;
	int VehicleAmmo;

	int w;
	int h;
}; extern CCVARS cvars;

struct CVehicle;

struct CServer
{
		char unknown0[876]; //0x0000
	__int32 spectate; //0x036C  
		char unknown880[4]; //0x0370
	__int32 premium; //0x0374  
	__int32 premium_; //0x0378  
};//Size=0x037C(892)

struct CPlayer
{
	vector recoil; //0x0000  
		char unknown12[16]; //0x000C
	float stamina; //0x001C  
		char unknown32[68]; //0x0020
	BYTE index; //0x0064  
		char unknown101[31]; //0x0065
	CVehicle* vehicle; //0x0084  
		char unknown136[16]; //0x0088
	WORD weapon__; //0x0098  
		char unknown154[130]; //0x009A
	BYTE rolling; //0x011C  
		char unknown285[11]; //0x011D
	float pitch; //0x0128  
		char unknown300[24]; //0x012C
	float yaw; //0x0144  
		char unknown328[8]; //0x0148
	vector pos_; //0x0150  
		char unknown348[12]; //0x015C
	WORD weapon; //0x0168  
	WORD weapon_; //0x016A  
		char unknown364[88]; //0x016C
	BYTE status; //0x01C4  
	BYTE status_; //0x01C5  
		char unknown454[90]; //0x01C6
	BYTE onground; //0x0220  
		char unknown545[27]; //0x0221
	BYTE forward; //0x023C  
	BYTE backward; //0x023D  
	BYTE left; //0x023E  
	BYTE wsad ; //0x023F  
		char unknown576[8]; //0x0240
	vector pos; //0x0248   
		char unknown596[192]; //0x0254
	float nofall; //0x0314  
		char unknown792[8]; //0x0318
	BYTE shooting; //0x0320  
};//Size=0x0330(816)


struct CBase
{
	CPlayer* local; //0x0000  
		char unknown4[72]; //0x0004
	CPlayer** player; //0x004C  
		char unknown80[48]; //0x0050
};//Size=0x0080(128)

struct CVehicle
{
		char unknown0[55]; //0x0000
	char name[18]; //0x0037  
};//Size=0x0049(73)

struct CServerBase
{
	CServer* server; //0x0000  
};//Size=0x0004(4)

struct CLocal
{
		char unknown0[32]; //0x0000
	vector pos; //0x0020  
		char unknown44[36]; //0x002C
	float pitch; //0x0050  
	float yaw; //0x0054  
		char unknown88[16]; //0x0058
	float view_down; //0x0068  
	float view_up; //0x006C  
	float fovx; //0x0070  
	float fovy; //0x0074  
};//Size=0x0078(120)

//struct CPlayerInfo
//{
//		char unknown0[12]; //0x0000
//	WORD local; //0x000C 257 = local? 
//		char unknown14[46]; //0x000E
//	char name[20]; //0x003C  
//		char unknown80[1004]; //0x0050
//	char ip[16]; //0x043C  
//		char unknown1100[140]; //0x044C
//	__int32 exp; //0x04D8  
//		char unknown1244[12]; //0x04DC
//	__int32 dinar; //0x04E8  
//		char unknown1260[64]; //0x04EC
//	char loginname[20]; //0x052C  
//		char unknown1344[492]; //0x0540
//	char name_[20]; //0x072C  
//		char unknown1856[4500]; //0x0740
//	__int32 points; //0x18D4  
//		char unknown6360[16]; //0x18D8
//	__int32 kills; //0x18E8  
//	__int32 deaths; //0x18EC  
//		char unknown6384[4]; //0x18F0
//	__int32 health; //0x18F4  
//		char unknown6392[140]; //0x18F8
//	BYTE index; //0x1984  
//		char unknown6533[3]; //0x1985
//	BYTE team; //0x1988  
//		char unknown6537[176]; //0x1989
//};//Size=0x1A39(6713)

struct CPlayerInfo
{
		char unknown0[12]; //0x0000
	WORD local; //0x000C 257 = local? 
		char unknown14[54]; //0x000E
	char name[20]; //0x0044  
		char unknown88[1004]; //0x0058
	char ip[16]; //0x0444  
		char unknown1108[140]; //0x0454
	__int32 exp; //0x04E0  
		char unknown1252[12]; //0x04E4
	__int32 dinar; //0x04F0  
		char unknown1268[64]; //0x04F4
	char loginname[20]; //0x0534  
		char unknown1352[492]; //0x0548
	char name_[20]; //0x0734  
		char unknown1864[4500]; //0x0748
	__int32 points; //0x18DC  
		char unknown6368[16]; //0x18E0
	__int32 kills; //0x18F0  
	__int32 deaths; //0x18F4  
		char unknown6392[4]; //0x18F8
	__int32 health; //0x18FC  
		char unknown6400[140]; //0x1900
	BYTE index; //0x198C  
	BYTE ID187A59E0; //0x198D  
	BYTE ID187A4360; //0x198E  
	BYTE ID187A4260; //0x198F  
	BYTE team; //0x1990  
		char unknown6545[176]; //0x1991
};//Size=0x1A41(6721)

#define ISLOCAL(k) (k->loginname[0])
#define ISVALID(k) (k->name[0] && k->ip[0])
#define ISALIVE(k) (ISVALID(k) && k->health > 0)

#endif