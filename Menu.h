#ifndef _MENU_
#define _MENU_

class CMenu
{
	char szTitel[32];
	struct elements_t
	{
		char szName[32];
		int iMin;
		int iMax;
		int iStep;
		int *pValue;
	};
	int iSelected;
	int x, y;
	int w;
	bool bOnHighlighting;
	bool bShowValue;

public:
	CMenu(char* szTitelName, int x, int y, int w, bool bInHighlighting, bool bShowValue);
	~CMenu();

	void AddEntry(char* szName, int* pValue, int iMin, int iMax, int iStep);
	bool KeyHandling();
	void Draw();
	elements_t list[50];
	bool bActive;
	bool bMenuActive;
	int iElements;
}; 
extern CMenu* pMenu;

#endif