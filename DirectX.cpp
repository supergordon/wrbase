#include "sdk.h"

_Reset					pReset					= NULL;
_EndScene				pEndScene				= NULL;
_SetTransform			pSetTransform			= NULL;
_DrawIndexedPrimitive	pDrawIndexedPrimitive	= NULL;
_SetStreamSource		pSetStreamSource		= NULL;

IDirect3DDevice8*		pD3Ddev					= NULL;
CCVARS					cvars					= {0};
UINT					STRIDE					= NULL;
IDirect3DTexture8*		texRed					= NULL;
IDirect3DTexture8*		texYellow				= NULL;

CLocal*					g_pView					= NULL;
CBase*					g_pBase					= NULL;
CServerBase*			g_pServer				= NULL;

CAddress*				g_pAddress				= new CAddress();



#define M_PI		3.14159265358979323846
#define M_PI_4      0.78539816339744830962
#define M_PI_2      1.57079632679489661923

void FlyToView(vector &pos)
{
	if(!g_pView) return;

	float yaw = g_pView->yaw / 57.295;
	float pitch = g_pView->pitch / 57.295;

	vector move;
	float Speed = 10.0;

	move.x = sinf(-yaw)*Speed;
	move.z = cosf(yaw)*Speed;
	move.y = sinf(-pitch)*Speed;

	if(pMenu->bMenuActive)
		return;
	//move.y = sin(-pitch)*Speed;

	if(GetAsyncKeyState(VK_LEFT)) {
		move.x = sinf(-yaw-M_PI_2)*Speed;
		move.z = cosf(yaw-M_PI_2)*Speed;
		pos += move;
	}
	if(GetAsyncKeyState(VK_RIGHT)) {
		move.x = sinf(-yaw+M_PI_2)*Speed;
		move.z = cosf(yaw+M_PI_2)*Speed;
		pos += move;
	}
	if(GetAsyncKeyState(VK_UP)) pos += move;
	if(GetAsyncKeyState(VK_DOWN)) pos -= move;
}

void Menu()
{
	if(GetAsyncKeyState(VK_INSERT) & 1)
		pMenu->bMenuActive = !pMenu->bMenuActive;

	if(pMenu->bMenuActive)
	{
		pMenu->Draw();
		pMenu->KeyHandling();

		pMenu->iElements = 0;

		pMenu->AddEntry("-DirectX", &cvars.D3D, 0, 1, 1);
		if(cvars.D3D)
		{
			pMenu->AddEntry(" Wallhack", &cvars.Wallhack, 0, 1, 1);
			pMenu->AddEntry(" Chams", &cvars.Chams, 0, 10, 1);
			pMenu->AddEntry(" Fullbright", &cvars.Fullbright, 0, 1, 1);
			pMenu->AddEntry(" Nofog", &cvars.Nofog, 0, 1, 1);
			pMenu->AddEntry(" Crosshair", &cvars.Crosshair, 0, 5, 1);
		}
		pMenu->AddEntry("-ESP", &cvars.ESP, 0, 1, 1);
		if(cvars.ESP)
		{
			pMenu->AddEntry(" Name", &cvars.Name, 0, 1, 1);
			pMenu->AddEntry(" Distance", &cvars.Distance, 0, 1, 1);
			pMenu->AddEntry(" Health", &cvars.Health, 0, 1, 1);
			pMenu->AddEntry(" IP", &cvars.IP, 0, 1, 1);
			pMenu->AddEntry(" State", &cvars.State, 0, 1, 1);
			pMenu->AddEntry(" Weapon", &cvars.Weapon, 0, 1, 1);
		}

		pMenu->AddEntry("-Player 1", &cvars.Player1, 0, 1, 1);
		if(cvars.Player1)
		{
			pMenu->AddEntry(" Stamina", &cvars.Stamina, 0, 2, 1);
			pMenu->AddEntry(" Nofall", &cvars.Nofall, 0, 1, 1);
			pMenu->AddEntry(" Superjump", &cvars.Superjump, 0, 255, 1);
			pMenu->AddEntry(" Nobounds", &cvars.Nobounds, 0, 1, 1);
			pMenu->AddEntry(" Nospawn", &cvars.NoSpawn, 0, 1, 1);
			pMenu->AddEntry(" Removals", &cvars.Removals, 0, 3, 1);
			pMenu->AddEntry(" Fly", &cvars.Fly, 0, 1, 1);
			pMenu->AddEntry(" Movement", &cvars.Movement, 0, 20, 1);
			pMenu->AddEntry(" Premium", &cvars.Premium, 0, 3, 1);
			pMenu->AddEntry(" Telekill", &cvars.Telekill, 0, 1, 1);
		}
		pMenu->AddEntry("-Player 2", &cvars.Player2, 0, 1, 1);
		if(cvars.Player2)
		{
			pMenu->AddEntry(" Antikick", &cvars.Antikick, 0, 1, 1);
			pMenu->AddEntry(" Invisible", &cvars.Invisible, 0, 1, 1);
			pMenu->AddEntry(" OPK", &cvars.OPK, 0, 1, 1);
			pMenu->AddEntry(" Unl Ammo", &cvars.UnlAmmo, 0, 1, 1);
			pMenu->AddEntry(" No Delay", &cvars.NoDelay, 0, 1, 1);
			pMenu->AddEntry(" Headshot", &cvars.Headshot, 0, 1, 1);
			pMenu->AddEntry(" Walk Thru", &cvars.WalkThru, 0, 1, 1);
			pMenu->AddEntry(" Shoot Thru", &cvars.ShootThru, 0, 1, 1);
		}
		pMenu->AddEntry("-Vehicle", &cvars.Vehicle, 0, 1, 1);
		if(cvars.Vehicle)
		{
			pMenu->AddEntry(" No Veh Dmg", &cvars.NoVehicleDamage, 0, 1, 1);
			pMenu->AddEntry(" Veh Jump", &cvars.VehicleJump, 0, 1, 1);
			pMenu->AddEntry(" Veh Invisible", &cvars.VehicleInvisible, 0, 1, 1);
			pMenu->AddEntry(" Veh Ammo", &cvars.VehicleAmmo, 0, 1, 1);
		}
	}
}

void __cdecl Reset(IDirect3DDevice8* pDevice, D3DPRESENT_PARAMETERS* pPP)
{
	__asm pushad

	pTools->ReleaseFont();

	__asm popad
	//return 0;//pReset(pDevice, pPP);
}



enum { NONE, VEHICLE, ROLLING, RUNNING, MOVING, STANDING, KNEEING, PRONE, LADDER, SWIMMING, JUMPING};
char* szStates[11] = { "NONE",  "VEHICLE", "ROLLING", "RUNNING", "MOVING", "STANDING", "KNEEING", "PRONE", "LADDER", "SWIMMING", "JUMPING"};

char *szWeaponList[120] = {
    "GunList", "DA_M7", "DA_KNUCKLE", "DA_STILETTO", "DA_SWORD", "DA_KNUCKLE_2", "DA_SQUEAKY_HAMMER", "DA_STILETTO_JP",
        "DA_SWORD_JP", "DB_COLT", "DB_DESERT_EG", "DB_MP5K", "DB_MAGNUM", "DB_GLOCK", "DB_BERETTA_D", "DB_THROWING_KNIFE",
        "DB_MICRO_UZI", "DB_BERETTA_M93R", "MDB_MP5K_GOLD", "DC_AK47", "DC_K2", "DC_M4A1", "DC_FAMAS", "DC_L85A1",
        "DC_XM8", "DC_TYPE89", "DC_SIG550", "DC_TAR_21", "DC_M16A4", "DC_AN94 _M16", "DC_HK417 _M16", "DC_M4A1_GOLD _M16",
        "DD_G36C", "DD_G36C_D", "DD_AKS74U _M16", "DE_G36", "DE_G36_D", "DF_MP5", "DF_P90", "DF_UZI", "DF_TMP9", "DF_K1",
        "DF_MP7A1", "DF_SCORPION_D", "DF_Spectre_M4", "DF_MAC10", "DF_UMP45", "DF_CX4STORM", "DF_K1_GOLD", "DG_PSG_1", "DG_BARRETT_M82",
        "DG_AUG", "DG_SSG", "DG_M24", "DG_DRAGUNOV_SVD", "DG_AI_AW", "DG_AW50F", "DG_M21", "DG_WA2000", "DG_SR25", "DG_M40A1",
        "DG_AI_AW_GOLD", "DH_M60", "DH_M249", "DI_WINCHESTER_1300", "DI_M4SUPER90", "DJ_PZF_3", "DJ_M136AT_4", "DJ_RPG_7", "DJ_JAVELIN",
        "DJ_RPG_7_GOLD", "DK_STINGER", "DK_STINGER", "DL_TMA_1A", "DL_HA_SUPPLY", "DM_K400_GRENADE", "DN_K400_GRENADE_ASSULT", "DO_SMOKE_G",
        "DO_FLASH_BANG_1", "DO_FLASH_BANG_2", "DP_CLAYMORE", "DP_CLAYMORE_SWITCH", "DP_PDA", "DP_SWITCH_C4", "DQ_MEDIC_KIT_1", "DQ_MEDIC_KIT_2",
        "DQ_MEDIC_KIT_3", "DR_SPANNER", "DR_PIPE_WRENCH", "DS_ADRENALINE", "DS_PARACHUTE", "DS_STAMINA", "DS_HP_KIT", "DS_DETECTOR",
        "DS_TELESCOPE", "DS_FLASH_MINE", "DT_MG3", "DT_M134", "DT_MK1S", "DT_HK69", "DU_AMMO_BOX", "DU_M14", "DU_TEARGAS",
        "DV_MEDIC_BOX", "DW_K203", "DW_TELESCOPE", "DW_SILENCER", "DU_NIPPERS", "???", "???", "D5_SCORPION_D_8TH", "D5_G36C_D_8TH",
        "D6_TMP9_8TH", "D6_MP7A1_8TH", "D7_AI_AW_8TH", "D7_BARRETT_M82_8TH", "D8_M60_8TH", "D8_G36_8TH", "D9_M249_8TH", "D9_M134_8TH"
};


BYTE GetState(CPlayer* Player)
{
    BYTE status = Player->status;
    BYTE status2 = Player->status_;

    if(Player->vehicle != 0)
        return VEHICLE;
    
    if(status == 0xA)
        return ROLLING;

    if(status == 0x8A)
        return RUNNING;
    
    if(status == 0x0D || status == 0x0E || status == 0x25 || status == 0x24)
        return MOVING;

    if((status == 0x40 && status2 == 0x01) /*|| status == 0x0D || status == 0x0E || status == 0x25 || status == 0x24*/ /*|| status == 0x8A || status == 0x0A*/)
        return STANDING;

    if(status == 0x26 || status == 0x27 || status == 0x28 || status == 0x3D || status == 0x3E)
        return KNEEING;

    if((status == 0x40 && status2 == 0x00) || status == 0x60 || status == 0x64 || status == 0x66 || status == 0x69)
        return PRONE;

    if(status == 0xDD || status == 0xD9)
        return LADDER;

    if(status == 0x47 || status == 0x45 || status == 0x44 || status == 0x46 || status == 0x4A)
        return SWIMMING;

    if(status == 0xD2 || status == 0xD1)
        return JUMPING;

    return 0;
}


int GetEspCorrection(CPlayer* pPlayer)
{
    int iState = GetState(pPlayer);
    
    if(iState == PRONE || iState == VEHICLE)
        return 8;

    if(iState == ROLLING || iState == SWIMMING)
        return 15;

    if(iState == KNEEING)
        return 19;

    if(iState == STANDING || iState == RUNNING|| iState == LADDER || iState == JUMPING || iState == MOVING)
        return 29;

    return 0;
}



D3DXMATRIX view, projection, world;
D3DVIEWPORT8 viewPort;

float GetDistance(CPlayer* pPlayer)
{
	vector a = g_pBase->local->pos;
	vector b = pPlayer->pos;

	return (b-a).Mag();
}

CPlayerInfo* GetPlayerInfo(int index)
{
	if(index > 32) return 0;
	DWORD dwBasePointer = 0xC1F4B8;
	return (CPlayerInfo*) (dwBasePointer + (index*0x1A48));
}

DWORD dwRetOPK = 0x6808D2;
CPlayer* pOPK = 0;
__declspec(naked) void OPK()
{
	__asm mov pOPK, ecx
	__asm pushad

	if(pOPK != g_pBase->local && (GetPlayerInfo(pOPK->index)->team != GetPlayerInfo(g_pBase->local->index)->team))
	{
		/*pOPK->pos = 0;
		pOPK->pos.y = 100;*/

		float yaw = g_pView->yaw / 57.295;
		float pitch = g_pView->pitch / 57.295;

		vector move;
		float Speed = 40.0;

		move.x = sinf(-yaw)*Speed;
		move.z = cosf(yaw)*Speed;
		vector pos = g_pBase->local->pos;
		pOPK->pos = (pos+move);
	}
	else
		pOPK->pos_ = pOPK->pos;

	__asm popad
	__asm jmp dwRetOPK
}

void __stdcall EndScene(IDirect3DDevice8* pDevice)
{
	__asm pushad

	static bool bInit = false;
	if(!bInit)
	{
		pTools->GenerateTexture(pDevice, &texRed, D3DCOLOR_ARGB(255, 255, 0, 0));
		pTools->GenerateTexture(pDevice, &texYellow, D3DCOLOR_ARGB(255, 255, 255, 0));
		bInit = true;
	}

	cvars.w = GetSystemMetrics(SM_CXSCREEN);
	cvars.h = GetSystemMetrics(SM_CYSCREEN);

	if(GetAsyncKeyState(VK_NUMPAD9) & 1)
	{
		cvars.Wallhack = 1;
		cvars.Crosshair = 3;
		cvars.Nofog = 1;
		cvars.Stamina = 1;
		cvars.Nofall = 1;
		cvars.Nobounds = 1;
		cvars.Superjump = 4;
		cvars.NoSpawn = 1;
		cvars.Antikick = 1;
		cvars.Premium = 3;
		cvars.Fly = 1;
		cvars.Invisible = 1;
		cvars.Removals = 3;
		cvars.Headshot = 1;
		cvars.Movement = 6;
		cvars.Name = 1;
		cvars.Health = 1;
	}

	g_pAddress->Restore();
	g_pAddress->Apply();
	
	g_pView = (CLocal*) *(DWORD*)0xB02154;
	pD3Ddev = pDevice;
	pTools->DrawString(10,10,255,0,0,255,DT_LEFT, "Hallo ...");

	if(!pFont || IsBadReadPtr((void*)pFont, 4))
		pTools->BuildFont();

	pTools->Crosshair(cvars.Crosshair);

	g_pBase = (CBase*)0xCC12A8;
	if(g_pBase->local)
	{
		//pTools->DrawString(10, 30, 255,0,0,255,DT_LEFT, "%s %.2f", szWeaponList[g_pBase->local->weapon], g_pBase->local->gravity);

		switch(cvars.Stamina)
		{
			case 1: g_pBase->local->stamina = 100; break;
			case 2: if(g_pBase->local->stamina <= 30) g_pBase->local->stamina = 30; break;
		}

		switch(cvars.Removals)
		{
			case 1: g_pBase->local->recoil = 0; break;
			case 2: *(int*)0xB02560 = 0; break;
			case 3: g_pBase->local->recoil = 0; *(int*)0xB02560 = 0; break;
		}

//		g_pBase->local->gravity = (float)cvars.PremiumCrosshair;

		if(cvars.Nofall)
			g_pBase->local->nofall = -2000;

		if(cvars.Superjump)
		{
			static float fHeight = 0;
			if(GetAsyncKeyState(cvars.Superjump))
			{
				if(fHeight == 0)
					fHeight = g_pBase->local->pos.y;

				fHeight += 10;
				g_pBase->local->pos.y = fHeight;
			}
			else fHeight = 0;
		}


		if(cvars.Fly)
		{
			vector pos = g_pBase->local->pos;
			FlyToView(pos);
			g_pBase->local->pos = pos;
		}

		if(cvars.Nobounds)
		{
			*(vector*)0xB3129C = 0;
		}

	}

	if(cvars.NoSpawn)
	{
		*(DWORD*)0xB1E370 = 0;
		*(DWORD*)0xB8026C = 0;
	}

	if(cvars.Movement)
	{
		float fSpeed = 96.0f * cvars.Movement;
		pTools->Patch(0x9A6C8C, (BYTE*)&fSpeed, 4);
	}

	/*try
	{*/
	g_pServer = (CServerBase*)0xBCA9D8;
	if(g_pServer->server)
	{
		g_pServer->server->premium = cvars.Premium;
		g_pServer->server->premium_ = cvars.Premium;

		int x = 0;
		int y = 0;
		static int myteam = 0;
		int iNearestIndex = 33;
		float fDist = 0;
		for(int i = 0; i < 32; i++)
		{
			if(!g_pBase->player || !g_pBase->local) break;
			CPlayerInfo *pInfo = GetPlayerInfo(i);
			CPlayer* pPlayer = g_pBase->player[i];
			if(pInfo && pPlayer)
			{
				
				if(ISLOCAL(pInfo))
				{
					pTools->DrawString(10,40,255,0,0,255,DT_LEFT, "%d", pInfo->index);
					myteam = pInfo->team;
				}
				if(pPlayer && ISALIVE(pInfo) && !ISLOCAL(pInfo) && GetState(pPlayer) != NONE)
				{
					if(cvars.Telekill)
					{
						if(!pPlayer->vehicle && myteam != pInfo->team)
						{
							float fBuf = GetDistance(pPlayer);
							if(fBuf > fDist || !fDist)
							{
								iNearestIndex = i;
								fDist = fBuf;
							}
						}
					}
					
					vector pos = pPlayer->pos;
					pos.y += GetEspCorrection(pPlayer);
					if(pTools->WorldToScreen(pos, x, y))
					{
						vector color = 255;
						if(myteam != pInfo->team)
							color = vector(255, 0, 0);
						else color = vector(255, 255, 255);

						int a = 0;
						if(cvars.Name)
						{
							pTools->DrawString(x, y+a, color.x, color.y, color.z, 255, DT_CENTER, "%s", pInfo->name);
							a += 10;
						}
						if(cvars.Distance)
						{
							vector me = g_pBase->local->pos;
							vector enemy = pPlayer->pos;
							float dist = (enemy-me).Mag();
							pTools->DrawString(x, y+a, color.x, color.y, color.z, 255, DT_CENTER, "%.2f", dist/35);
							a += 10;
						}
						if(cvars.Health)
						{
							pTools->DrawString(x, y+a, color.x, color.y, color.z, 255, DT_CENTER, "%d%%", pInfo->health/10);
							a += 10;
						}
						if(cvars.IP)
						{
							pTools->DrawString(x, y+a, color.x, color.y, color.z, 255, DT_CENTER, "%s", pInfo->ip);
							a += 10;
						}
						if(cvars.State)
						{
							pTools->DrawString(x, y+a, color.x, color.y, color.z, 255, DT_CENTER, "%s", szStates[GetState(pPlayer)]);
							a += 10;
						}
						if(cvars.Weapon)
						{
							pTools->DrawString(x, y+a, color.x, color.y, color.z, 255, DT_CENTER, "%s", "not implemented yet");
							a += 10;
						}
					}
				}
			}
		}
	
		if(cvars.Telekill)
		{
			if(g_pBase->player && g_pBase->local)
			{
				CPlayerInfo *pInfo = GetPlayerInfo(iNearestIndex);
				CPlayer* pPlayer = g_pBase->player[iNearestIndex];

				if(pInfo && pPlayer)
				{
					float yaw = pPlayer->yaw / 57.295;
					float pitch = pPlayer->pitch / 57.295;

					vector move;
					float Speed = 15.0;

					move.x = sinf(-yaw)*Speed;
					move.z = cosf(yaw)*Speed;
					g_pBase->local->pos = (pPlayer->pos+move);
				}
			}
		}
	}
	//}
	//catch(...)
	//{
	//	__asm popad
	//	return;
	//}

	Menu();

	__asm popad
	//return 0;//pEndScene(pDevice);
}

void __cdecl SetTransform(IDirect3DDevice8* pDevice, D3DTRANSFORMSTATETYPE State, D3DMATRIX* pMatrix)
{
	__asm pushad

	if(State == D3DTS_VIEW) view = *pMatrix;
	if(State == D3DTS_PROJECTION) projection = *pMatrix;
	if(State == D3DTS_WORLD) world = *pMatrix;

	__asm popad
//	return 0;//pSetTransform(pDevice, State, pMatrix);
}

void __cdecl DrawIndexedPrimitive(IDirect3DDevice8* pDevice, D3DPRIMITIVETYPE type, UINT minIndex, UINT NumVertices, UINT startIndex, UINT primCount)
{
	__asm pushad

	if(cvars.Nofog) pDevice->SetRenderState(D3DRS_FOGENABLE, false);

	if(cvars.Fullbright)
		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	else
		pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	if(STRIDE == 44)
	{
		if(cvars.Wallhack)
		{
			pDevice->SetRenderState(D3DRS_ZENABLE, false);
			pDevice->SetRenderState(D3DRS_FILLMODE,D3DFILL_SOLID);
			pDevice->SetRenderState(D3DRS_FOGENABLE, false);
			pDevice->DrawIndexedPrimitive(type, minIndex, NumVertices, startIndex, primCount);
			pDevice->SetRenderState(D3DRS_ZENABLE, true);
		}

		if(cvars.Chams)
		{
			pDevice->SetRenderState(D3DRS_FOGENABLE, false);
			pDevice->SetRenderState(D3DRS_ZENABLE, false);
			pDevice->SetTexture(0, texYellow);
			pDevice->DrawIndexedPrimitive(type, minIndex, NumVertices, startIndex, primCount);
			pDevice->SetRenderState(D3DRS_ZENABLE, true);
			pDevice->SetTexture(0, texRed);
			pDevice->SetRenderState(D3DRS_FILLMODE,D3DFILL_SOLID);
		}
	}
	__asm popad
	//return 0;//pDrawIndexedPrimitive(pDevice, type, minIndex, NumVertices, startIndex, primCount);
}

void __cdecl SetStreamSource(IDirect3DDevice8* pDevice, UINT StreamNumber, IDirect3DVertexBuffer8* pStreamData, UINT Stride)
{
	__asm pushad

	if(StreamNumber == 0) 
		STRIDE = Stride;

	__asm popad
	//return 0;//pSetStreamSource(pDevice, StreamNumber, pStreamData, Stride);
}
