#include "sdk.h"

void HideModule(HINSTANCE hModule)
{
	DWORD dwPEB_LDR_DATA = 0;
	_asm
	{
		pushad;
		pushfd;
		mov eax, fs:[30h]		   // PEB
		mov eax, [eax+0Ch]		  // PEB->ProcessModuleInfo
		mov dwPEB_LDR_DATA, eax	 // Save ProcessModuleInfo

InLoadOrderModuleList:
		mov esi, [eax+0Ch]					  // ProcessModuleInfo->InLoadOrderModuleList[FORWARD]
		mov edx, [eax+10h]					  //  ProcessModuleInfo->InLoadOrderModuleList[BACKWARD]

		LoopInLoadOrderModuleList: 
		    lodsd							   //  Load First Module
			mov esi, eax		    			//  ESI points to Next Module
			mov ecx, [eax+18h]		    		//  LDR_MODULE->BaseAddress
			cmp ecx, hModule		    		//  Is it Our Module ?
			jne SkipA		    		    	//  If Not, Next Please (@f jumps to nearest Unamed Lable @@:)
		    	mov ebx, [eax]				  //  [FORWARD] Module 
		    	mov ecx, [eax+4]    		    	//  [BACKWARD] Module
		    	mov [ecx], ebx				  //  Previous Module\'s [FORWARD] Notation, Points to us, Replace it with, Module++
		    	mov [ebx+4], ecx			    //  Next Modules, [BACKWARD] Notation, Points to us, Replace it with, Module--
			jmp InMemoryOrderModuleList		//  Hidden, so Move onto Next Set
		SkipA:
			cmp edx, esi					    //  Reached End of Modules ?
			jne LoopInLoadOrderModuleList		//  If Not, Re Loop

InMemoryOrderModuleList:
		mov eax, dwPEB_LDR_DATA		  //  PEB->ProcessModuleInfo
		mov esi, [eax+14h]			   //  ProcessModuleInfo->InMemoryOrderModuleList[START]
		mov edx, [eax+18h]			   //  ProcessModuleInfo->InMemoryOrderModuleList[FINISH]

		LoopInMemoryOrderModuleList: 
			lodsd
			mov esi, eax
			mov ecx, [eax+10h]
			cmp ecx, hModule
			jne SkipB
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp InInitializationOrderModuleList
		SkipB:
			cmp edx, esi
			jne LoopInMemoryOrderModuleList

InInitializationOrderModuleList:
		mov eax, dwPEB_LDR_DATA				    //  PEB->ProcessModuleInfo
		mov esi, [eax+1Ch]						 //  ProcessModuleInfo->InInitializationOrderModuleList[START]
		mov edx, [eax+20h]						 //  ProcessModuleInfo->InInitializationOrderModuleList[FINISH]

		LoopInInitializationOrderModuleList: 
			lodsd
			mov esi, eax		
			mov ecx, [eax+08h]
			cmp ecx, hModule		
			jne SkipC
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp Finished
		SkipC:
			cmp edx, esi
			jne LoopInInitializationOrderModuleList

		Finished:
			popfd;
			popad;
	}
}

DWORD WINAPI HookThread(LPVOID);

BOOL __stdcall DllMain(HMODULE kk, DWORD dwReason, LPVOID)
{
	if(dwReason == DLL_PROCESS_ATTACH)
	{
		HideModule(kk);
		CreateThread(0, 0, HookThread, 0, 0, 0);
		return true;
	}
	return FALSE;
}

DWORD dwEax = 0;
DWORD dwEcx = 0;
DWORD dwEsp = 0;
DWORD dwEspPtr = 0;

__declspec(naked) void EndScene_CallGate()
{
	__asm {
		mov dwEax, eax
		mov dwEcx, ecx
		mov dwEsp, esp
		push eax
		mov eax, dword ptr [esp+4]
		mov dwEspPtr, eax
		pop eax
	}
	
	/*__asm pushad
		pTools->LogText("EAX: %08X    ECX: %08X    ESP: %08X    pESP: %08X", dwEax, dwEcx, dwEsp, dwEspPtr);
	__asm popad*/

	__asm 
	{
		call EndScene
		push dwEax
		call dword ptr [ecx+0x8C]
		retn
	}
}

__declspec(naked) void SetStreamSource_GallGate()
{
	__asm
	{
		call SetStreamSource
		call dword ptr [ecx+0x14C]
		pop esi
		retn 0x10
	}
}

__declspec(naked) void DrawIndexedPrimitive_GallGate()
{
	__asm
	{
		call DrawIndexedPrimitive
		call dword ptr [esi+0x11C]
		pop esi
		retn 0x18
	}
}

__declspec(naked) void SetTransform_GallGate()
{
	__asm 
	{
		call SetTransform
		call dword ptr [ecx+0x94]
		retn 8
	}
}

DWORD dwRet = 0x7DF39D + 5;
__declspec(naked) void Reset_GallGate()
{
	__asm 
	{
		call Reset
		call dword ptr [ecx+0x38]
		test eax, eax
		jmp dwRet
	}
}

void RestoreD3D()
{
	pTools->Patch(0x7DA3D6, pHackshield->bEndScene, 5);
	pTools->Patch(0x7DABA5, pHackshield->bSetStreamSource, 5);
	pTools->Patch(0x7DA2F0, pHackshield->bDrawIndexedPrimitive, 5);
	//pTools->Patch(0x7D2CC0, pHackshield->bSetTransform, 5);
	pTools->Patch(0x7DF39D, pHackshield->bReset, 5);
}

void RehookD3D()
{
	pTools->DetourFunc((BYTE*)0x7DA3D6, (PBYTE)EndScene_CallGate, 5);
	pTools->DetourFunc((PBYTE)0x7DABA5, (PBYTE)SetStreamSource_GallGate, 5);
	pTools->DetourFunc((PBYTE)0x7DA2F0, (PBYTE)DrawIndexedPrimitive_GallGate, 5);
	//pTools->DetourFunc((PBYTE)0x7D2CC0, (PBYTE)SetTransform_GallGate, 5);
	pTools->DetourFunc((PBYTE)0x7DF39D, (PBYTE)Reset_GallGate, 5);
}

extern void OPK();

DWORD dwAddress = 0;

DWORD WINAPI HookThread(LPVOID)
{
	Sleep(10000);

	//DWORD dwDevicepointer = NULL;
	//while(dwDevicepointer == NULL) {
	//	dwDevicepointer = *(DWORD*)0xAF5058;
	//	Sleep(100);
	//}

	//DWORD dwEAX = NULL;
	//while(dwEAX == NULL) {
	//	dwEAX = *(DWORD*)(dwDevicepointer + 0x48);
	//	Sleep(100);
	//}

	//DWORD dwECX = *(DWORD*)dwEAX;
	//pdwVMT = (DWORD*)dwECX;



	memcpy(&pHackshield->bEndScene, (void*)0x7DA3D6, 5);
	memcpy(&pHackshield->bSetStreamSource, (void*)0x7DABA5, 5);
	memcpy(&pHackshield->bDrawIndexedPrimitive, (void*)0x7DA2F0, 5);
	//memcpy(&pHackshield->bSetTransform, (void*)0x7D2CC0, 5);
	memcpy(&pHackshield->bReset, (void*)0x7DF39D, 5);

	pTools->DetourFunc((BYTE*)0x7DA3D6, (PBYTE)EndScene_CallGate, 5);
	pTools->DetourFunc((PBYTE)0x7DABA5, (PBYTE)SetStreamSource_GallGate, 5);
	pTools->DetourFunc((PBYTE)0x7DA2F0, (PBYTE)DrawIndexedPrimitive_GallGate, 5);
	//pTools->DetourFunc((PBYTE)0x7D2CC0, (PBYTE)SetTransform_GallGate, 5);
	pTools->DetourFunc((PBYTE)0x7DF39D, (PBYTE)Reset_GallGate, 5);

	g_pAddress->Add(0x4C0EEF, &cvars.Antikick,  (PBYTE)"\xEB", (PBYTE)"\x75", 1);
	//g_pAddress->Add(0x68D361, &cvars.Invisible, (PBYTE)"\x1C", (PBYTE)"\x2C", 1);

	/*g_pAddress->Add(0x4E3FD0, &cvars.OPK, (PBYTE)"\xC7\x81\x3C\x02\x00\x00\x00\x00\x00\x00\xC7\x81\x40\x02\x00\x00\x00\x00\x00\x00\xC7\x81\x44\x02\x00\x00\x00\x00\x00\x00\xEB\x22", 
										  (PBYTE)"\x8B\x81\x3C\x02\x00\x00\x89\x81\x44\x01\x00\x00\x8B\x91\x40\x02\x00\x00\x89\x91\x48\x01\x00\x00\x8B\x81\x44\x02\x00\x00\x8B\x54",
		32);*/

	g_pAddress->Add(0x689600, &cvars.UnlAmmo, (PBYTE)"\xC2\x04\x00", (PBYTE)"\x56\x8B\xF1", 3);
	g_pAddress->Add(0x6A0CB4, &cvars.NoDelay, (PBYTE)"\x90\x90", (PBYTE)"\x74\x43", 2);
	//g_pAddress->Add(0x48F766, &cvars.Headshot, (PBYTE)"\x90\x90", (PBYTE)"\x75\x0F", 2); //0047D8A0  /$  56                   PUSH ESI
//	g_pAddress->Add(0x47D8B2, &cvars.Headshot, (PBYTE)"\x90\x90\x90\x90\x90\x90", (PBYTE)"\x0F\x84\x52\x02\x00\x00", 6);
//	g_pAddress->Add(0x47D8C8, &cvars.Headshot, (PBYTE)"\xE9\x3D\x02\x00\x00\x90", (PBYTE)"\x0F\x84\x3C\x02\x00\x00", 6);


	g_pAddress->Add(0x78AA37, &cvars.WalkThru, (PBYTE)"\x90\x90\x90", (PBYTE)"\xD8\x4A\x08", 3);
	g_pAddress->Add(0x6AB406, &cvars.ShootThru, (PBYTE)"\x90\x90\x90", (PBYTE)"\x8B\x51\x38", 3); //006AB2C6  |.  8B51 38              MOV EDX,DWORD PTR DS:[ECX+38]



	g_pAddress->Add(0x44C138, &cvars.NoVehicleDamage, (PBYTE)"\xE9\x25\x01\x00\x00\x90", (PBYTE)"\x0F\x84\x24\x01\x00\x00", 6);
	g_pAddress->Add(0x645FCE, &cvars.VehicleJump, (PBYTE)"\x90\x90", (PBYTE)"\x74\x7B", 2);
	g_pAddress->Add(0x4A8664, &cvars.VehicleJump, (PBYTE)"\x90\x90\x90\x90\x90\x90", (PBYTE)"\x0F\x85\xCE\x04\x00\x00", 6);
	g_pAddress->Add(0x451E42, &cvars.VehicleInvisible, (PBYTE)"\x1C", (PBYTE)"\x0C", 1);
	g_pAddress->Add(0x44CFDD, &cvars.VehicleAmmo, (PBYTE)"\x90\x90", (PBYTE)"\x75\x33", 2);
	g_pAddress->Add(0x44CFF0, &cvars.VehicleAmmo, (PBYTE)"\xEB\x20", (PBYTE)"\x8B\x15", 2);

	g_pAddress->AddNakedHook(0x680890, &cvars.OPK, OPK, (PBYTE)"\x8B\x81\x48\x02\x00");
	//g_pAddress->Add(0x66C3FA, &cvars.PremiumCrosshair, (PBYTE)"\x90\x90", (PBYTE)"\x75\x0A", 2);
	pHackshield->Hook();

	//dwAddress = (DWORD)VirtualAlloc(0, 0x1000, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	//BYTE Buffer[0x1000] = {0};
	//memcpy((void*)dwAddress, (void*)0x401000, 0x1000);
	//pTools->LogText("%X", dwAddress);

	return TRUE;
}

/*

004E3FD0  /$  8B81 3C020000       MOV EAX,DWORD PTR DS:[ECX+23C]
004E3FD6  |.  8981 44010000       MOV DWORD PTR DS:[ECX+144],EAX
004E3FDC  |.  8B91 40020000       MOV EDX,DWORD PTR DS:[ECX+240]
004E3FE2  |.  8991 48010000       MOV DWORD PTR DS:[ECX+148],EDX
004E3FE8  |.  8B81 44020000       MOV EAX,DWORD PTR DS:[ECX+244]
004E3FEE  |.  8B5424 04           MOV EDX,DWORD PTR SS:[ESP+4]
004E3FF2  |.  8981 4C010000       MOV DWORD PTR DS:[ECX+14C],EAX
004E3FF8  |.  8B4424 08           MOV EAX,DWORD PTR SS:[ESP+8]
004E3FFC  |.  8991 3C020000       MOV DWORD PTR DS:[ECX+23C],EDX


*/

/*
endscene
DR0 on execution at WarRock.7D243C

007DA3D6  |.  FF91 8C000000 CALL DWORD PTR DS:[ECX+8C]

007D2430  /$  8B41 48       MOV EAX,DWORD PTR DS:[ECX+48]
007D2433  |.  8B08          MOV ECX,DWORD PTR DS:[EAX]
007D2435  |.  50            PUSH EAX
007D2436  |.  FF91 8C000000 CALL DWORD PTR DS:[ECX+8C]
007D243C  \.  C3            RETN


reset
DR1 on execution at WarRock.7D7400 +5
007DF39D   .  FF51 38       CALL DWORD PTR DS:[ECX+38]


007D73FD  |.  FF51 38       CALL DWORD PTR DS:[ECX+38]
007D7400  |.  85C0          TEST EAX,EAX
007D7402  |.  0F8C CC000000 JL warrock.007D74D4
007D7408  |.  8B46 48       MOV EAX,DWORD PTR DS:[ESI+48]
007D740B  |.  8B08          MOV ECX,DWORD PTR DS:[EAX]
007D740D  |.  8D5424 04     LEA EDX,DWORD PTR SS:[ESP+4]


settransform
DR0 on execution at WarRock.7D2CC6
007DAC60  |.  FF91 94000000 CALL DWORD PTR DS:[ECX+94]


007D2CB0  /$  8B5424 08     MOV EDX,DWORD PTR SS:[ESP+8]
007D2CB4  |.  8B41 48       MOV EAX,DWORD PTR DS:[ECX+48]
007D2CB7  |.  8B08          MOV ECX,DWORD PTR DS:[EAX]
007D2CB9  |.  52            PUSH EDX
007D2CBA  |.  8B5424 08     MOV EDX,DWORD PTR SS:[ESP+8]
007D2CBE  |.  52            PUSH EDX
007D2CBF  |.  50            PUSH EAX
007D2CC0  |.  FF91 94000000 CALL DWORD PTR DS:[ECX+94]
007D2CC6  \.  C2 0800       RETN 8


dip
007DA2F0  |.  FF96 1C010000 CALL DWORD PTR DS:[ESI+11C]


DR0 on execution at WarRock.7D2356
007D2320  /$  8B4424 04     MOV EAX,DWORD PTR SS:[ESP+4]
007D2324  |.  8B5424 18     MOV EDX,DWORD PTR SS:[ESP+18]
007D2328  |.  56            PUSH ESI
007D2329  |.  8BB481 A00B03>MOV ESI,DWORD PTR DS:[ECX+EAX*4+30BA0]
007D2330  |.  52            PUSH EDX
007D2331  |.  03F2          ADD ESI,EDX
007D2333  |.  8B5424 1C     MOV EDX,DWORD PTR SS:[ESP+1C]
007D2337  |.  52            PUSH EDX
007D2338  |.  8B5424 1C     MOV EDX,DWORD PTR SS:[ESP+1C]
007D233C  |.  52            PUSH EDX
007D233D  |.  8B5424 1C     MOV EDX,DWORD PTR SS:[ESP+1C]
007D2341  |.  52            PUSH EDX
007D2342  |.  89B481 A00B03>MOV DWORD PTR DS:[ECX+EAX*4+30BA0],ESI
007D2349  |.  8B49 48       MOV ECX,DWORD PTR DS:[ECX+48]
007D234C  |.  8B31          MOV ESI,DWORD PTR DS:[ECX]
007D234E  |.  50            PUSH EAX
007D234F  |.  51            PUSH ECX
007D2350  |.  FF96 1C010000 CALL DWORD PTR DS:[ESI+11C]
007D2356  |.  5E            POP ESI
007D2357  \.  C2 1800       RETN 18


sss
007DABA5  |.  FF91 4C010000 CALL DWORD PTR DS:[ECX+14C]


DR0 on execution at WarRock.7D2C0B
007D2BE0  /$  8B5424 08     MOV EDX,DWORD PTR SS:[ESP+8]
007D2BE4  |.  85D2          TEST EDX,EDX
007D2BE6  |.  75 08         JNZ SHORT warrock.007D2BF0
007D2BE8  |.  B8 05400080   MOV EAX,80004005
007D2BED  |.  C2 1000       RETN 10
007D2BF0  |>  8B52 04       MOV EDX,DWORD PTR DS:[EDX+4]
007D2BF3  |.  8B41 48       MOV EAX,DWORD PTR DS:[ECX+48]
007D2BF6  |.  8B08          MOV ECX,DWORD PTR DS:[EAX]
007D2BF8  |.  56            PUSH ESI
007D2BF9  |.  8B7424 14     MOV ESI,DWORD PTR SS:[ESP+14]
007D2BFD  |.  56            PUSH ESI
007D2BFE  |.  52            PUSH EDX
007D2BFF  |.  8B5424 10     MOV EDX,DWORD PTR SS:[ESP+10]
007D2C03  |.  52            PUSH EDX
007D2C04  |.  50            PUSH EAX
007D2C05  |.  FF91 4C010000 CALL DWORD PTR DS:[ECX+14C]
007D2C0B  |.  5E            POP ESI
007D2C0C  \.  C2 1000       RETN 10
*/