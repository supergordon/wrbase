#ifndef _DIRECTX_
#define _DIRECTX_

void __cdecl Reset(IDirect3DDevice8* pDevice, D3DPRESENT_PARAMETERS* pPP);
void __stdcall EndScene(IDirect3DDevice8* pDevice);
void __cdecl SetTransform(IDirect3DDevice8* pDevice, D3DTRANSFORMSTATETYPE State, D3DMATRIX* pMatrix);
void __cdecl DrawIndexedPrimitive(IDirect3DDevice8* pDevice, D3DPRIMITIVETYPE type, UINT minIndex, UINT NumVertices, UINT startIndex, UINT primCount);
void __cdecl SetStreamSource(IDirect3DDevice8* pDevice, UINT StreamNumber, IDirect3DVertexBuffer8* pStreamData, UINT Stride);

typedef long (__stdcall* _Reset)(IDirect3DDevice8* pDevice, D3DPRESENT_PARAMETERS* pPP);
typedef long (__stdcall* _EndScene)(IDirect3DDevice8* pDevice);
typedef long (__stdcall* _SetTransform)(IDirect3DDevice8* pDevice, D3DTRANSFORMSTATETYPE State, D3DMATRIX* pMatrix);
typedef long (__stdcall* _DrawIndexedPrimitive)(IDirect3DDevice8* pDevice, D3DPRIMITIVETYPE type, UINT minIndex, UINT NumVertices, UINT startIndex, UINT primCount);
typedef long (__stdcall* _SetStreamSource)(IDirect3DDevice8* pDevice, UINT StreamNumber, IDirect3DVertexBuffer8* pStreamData, UINT Stride);

extern _Reset					pReset;
extern _EndScene				pEndScene;
extern _SetTransform			pSetTransform;
extern _DrawIndexedPrimitive	pDrawIndexedPrimitive;
extern _SetStreamSource			pSetStreamSource;

extern IDirect3DDevice8*		pD3Ddev;

void RestoreD3D();
void RehookD3D();

#endif