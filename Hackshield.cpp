#include "sdk.h"

CHackshield* pHackshield = new CHackshield;

HSSELFScan pSelfScan = 0;
HSD3DScan pD3DScan = 0;
HSMEMScan pMemScan = 0;
HSD3DVMTScan pVMTScan = 0;

CCVARS gVars = {0};
void CHackshield::TurnOnOff(bool toggle)
{
	if(!toggle)
	{	
		//restore all stuff
		memcpy(&gVars, &cvars, sizeof(CCVARS));
		memset(&cvars, 0, sizeof(CCVARS));
		g_pAddress->Restore();
	}
	else if(toggle)
	{
		//reapply
		memcpy(&cvars, &gVars, sizeof(CCVARS));
		g_pAddress->Apply();
	}
}

//int __cdecl hkVMTScan(int param1)
//{
//	__asm pushad
//		int k = 0;
//		//restore vmt hooks
//		RestoreD3D();
//	__asm popad
//
//	//k = pVMTScan(param1);
//	__asm {
//		push param1
//		call pVMTScan
//		add esp, 4
//		mov k, eax
//	}
//
//	__asm pushad
//		pTools->LogText("end %d", k);
//		//rehook
//		RehookD3D();
//	__asm popad
//
//	return k;
//}

extern DWORD dwAddress;
int __stdcall hkMEMScan(DWORD param1, DWORD param2, DWORD param3)
{
	__asm pushad
		pHackshield->TurnOnOff(false);
	pTools->LogText("%X", param1);
		if(param1 >= 0x7D0000 && param1 <= 0x7E0000)
		{
			RestoreD3D();
		}

		int k = 0;

		__asm {
			popad
			push param3
			push param2
			push param1
			call pMemScan
			mov k, eax
			pushad
		}

		pHackshield->TurnOnOff(true);
		if(param1 >= 0x7D0000 && param1 <= 0x7E0000)
		{
			RehookD3D();
		}

	__asm popad
	return k;
}

int __stdcall hkSelfScan(DWORD param1, DWORD param2, DWORD param3, DWORD param4)
{
	__asm pushad
		int k = 0;
		pHackshield->RestoreHooks();
		pHackshield->TurnOnOff(false); //zur sicherheit
		pTools->LogText("keke");
	__asm popad
	
	pSelfScan = (HSSELFScan)pHackshield->dwSELFScan;
	//k = pSelfScan(param1, param2, param3, param4);
	__asm 
	{
		push param4
		push param3
		push param2
		push param1
		call pSelfScan
		mov k, eax
	}
	
	__asm pushad
		pHackshield->Rehook();
		pHackshield->TurnOnOff(true);
	__asm popad

	return k;
}

//int __cdecl hkD3DScan(DWORD param1, DWORD param2, DWORD param3, DWORD param4)
//{
//	__asm pushad
//		int k = 0;
//		RestoreD3D();
//	_asm popad
//
//	k = pD3DScan(param1, param2, param3, param4);
//
//	__asm pushad
//		RehookD3D();
//	__asm popad
//
//	return k;
//}


typedef int (__cdecl* DllScanOne)(int param1, char* param2, char* param3);
DllScanOne pDllScanOne = NULL;

int __cdecl hkDllScanOne(int pid, char* processpath, int unk)
{
	return 0x13371337; //fake process base
}

void CHackshield::Hook()
{
	Sleep(1000);
	DWORD dwHS = (DWORD)GetModuleHandle("EHSvc.dll");


	//64 89 25 00 00 00 00 81 C4 D4 FD FF FF -0x16
	/*dwVMTScan = pTools->dwFindPattern(dwHS, 0x90000, PBYTE("\x64\x89\x25\x00\x00\x00\x00\x81\xC4\xD4\xFD\xFF\xFF"), "xxxxxxxxxxxxx") - 0x16;
	memcpy(&bVMTScan, (void*)dwVMTScan, 6);
	pVMTScan = (HSD3DVMTScan)pTools->DetourFunc((PBYTE)dwVMTScan, (PBYTE)hkVMTScan, 5);*/

	
	//83 EC 08 B8 2C 12 00 00 -0x1D
	dwMEMScan = pTools->dwFindPattern(dwHS, 0x90000, PBYTE("\x83\xEC\x08\xB8\x2C\x12\x00\x00"), "xxxxxxxx") - 0x1D;
	memcpy(&bMEMScan, (void*)dwMEMScan, 6);
	pMemScan = (HSMEMScan)pTools->DetourFunc((PBYTE)dwMEMScan, (PBYTE)hkMEMScan, 5);

	//55 8B EC 83 EC 08 53 89 4D F8
	dwSELFScan = pTools->dwFindPattern(dwHS, 0x90000, PBYTE("\x55\x8B\xEC\x83\xEC\x08\x53\x89\x4D\xF8"), "xxxxxxxxxx"); //dwHS+0x5140D;
	memcpy(&bSELFScan, (void*)dwSELFScan, 6);
	pSelfScan = (HSSELFScan)pTools->DetourFunc((PBYTE)dwSELFScan, (PBYTE)hkSelfScan, 6);

	//83 C4 D4 53 56 57 89 65 -0x1D
	/*dwD3DScan = dwHS+0x5206C;
	memcpy(&bD3DScan, (void*)dwD3DScan, 6);
	pD3DScan = (HSD3DScan)pTools->DetourFunc((PBYTE)dwD3DScan, (PBYTE)hkD3DScan, 5);*/

	//DWORD k = dwHS+0x20660;
	//pDllScanOne = (DllScanOne)pTools->DetourFunc((PBYTE)k, (PBYTE)hkDllScanOne, 10);
}

/*
1001FC10    83EC 0C         SUB ESP,0C
2 params, cdecl
8

10020660    8B4424 0C       MOV EAX,DWORD PTR SS:[ESP+C]
3 params, cdecl
10

Process & DLL Scans
*/

void CHackshield::RestoreHooks()
{
	//pTools->Patch(dwVMTScan, bVMTScan, 6);
	pTools->Patch(dwMEMScan, bMEMScan, 6);
	pTools->Patch(dwSELFScan, bSELFScan, 6);
	//pTools->Patch(dwD3DScan, bD3DScan, 6);
}

void CHackshield::Rehook()
{
	//pVMTScan = (HSD3DVMTScan)pTools->DetourFunc((PBYTE)dwVMTScan, (PBYTE)hkVMTScan, 5);
	pMemScan = (HSMEMScan)pTools->DetourFunc((PBYTE)dwMEMScan, (PBYTE)hkMEMScan, 5);
	pSelfScan = (HSSELFScan)pTools->DetourFunc((PBYTE)dwSELFScan, (PBYTE)hkSelfScan, 6);
	//pD3DScan = (HSD3DScan)pTools->DetourFunc((PBYTE)dwD3DScan, (PBYTE)hkD3DScan, 5);
}