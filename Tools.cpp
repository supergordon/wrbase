#include "sdk.h"

CTools* pTools = new CTools;

ID3DXFont* pFont = NULL;

void CTools::LogText(char* szText, ...)
{
	char szBuffer[1024];
	va_list args;
    va_start(args, szText);
    memset(szBuffer, 0, sizeof(szBuffer));
    _vsnprintf(szBuffer, sizeof(szBuffer) - 1, szText, args);
    va_end(args);

	std::ofstream LOG("C:\\wr.txt",std::ios::app);
	LOG << szBuffer << std::endl;
	LOG.close();
}

void *CTools::DetourFunc(BYTE *src, const BYTE *dst, const int len)
{
	BYTE *jmp = (BYTE*)malloc(len+5);
	DWORD dwback;

	VirtualProtect(src, len, PAGE_READWRITE, &dwback);

	memcpy(jmp, src, len);	jmp += len;
	
	jmp[0] = 0xE9;
	*(DWORD*)(jmp+1) = (DWORD)(src+len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src+1) = (DWORD)(dst - src) - 5;

	VirtualProtect(src, len, dwback, &dwback);

	return (jmp-len);
}

bool CTools::RetourFunc(BYTE *src, BYTE *restore, const int len)
{
	DWORD dwback;
		
	if(!VirtualProtect(src, len, PAGE_READWRITE, &dwback))	{ return false; }
	if(!memcpy(src, restore, len))							{ return false; }

	restore[0] = 0xE9;
	*(DWORD*)(restore+1) = (DWORD)(src - restore) - 5;

	if(!VirtualProtect(src, len, dwback, &dwback))			{ return false; }
	
	return true;
}

bool bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for(;*szMask;++szMask,++pData,++bMask)
		if(*szMask=='x' && *pData!=*bMask ) 
			return false;
	return (*szMask) == NULL;
}

DWORD CTools::dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask)
{
	for(DWORD i=0; i < dwLen; i++)
		if( bDataCompare( (BYTE*)( dwAddress+i ),bMask,szMask) )
			return (DWORD)(dwAddress+i);

	return 0;
}

void CTools::Patch(DWORD dwAddress, BYTE* lala, int size)
{
	DWORD dwProtect;
	VirtualProtect((void*)dwAddress, size, PAGE_EXECUTE_READWRITE, &dwProtect);
	memcpy((void*)dwAddress, lala, size);
	VirtualProtect((void*)dwAddress, size, dwProtect, 0);
}

HRESULT CTools::GenerateTexture(LPDIRECT3DDEVICE8 pDevice,IDirect3DTexture8 **ppD3Dtex, DWORD colour32)
{
	if( FAILED(pDevice->CreateTexture(8, 8, 1, 0, D3DFMT_A4R4G4B4, D3DPOOL_MANAGED, ppD3Dtex)) )
		return E_FAIL;

	WORD colour16 =    ((WORD)((colour32>>28)&0xF)<<12)
		|(WORD)(((colour32>>20)&0xF)<<8)
		|(WORD)(((colour32>>12)&0xF)<<4)
		|(WORD)(((colour32>>4)&0xF)<<0);

	D3DLOCKED_RECT d3dlr;    
	(*ppD3Dtex)->LockRect(0, &d3dlr, 0, 0);
	WORD *pDst16 = (WORD*)d3dlr.pBits;

	for(int xy=0; xy < 64; xy++)
		*pDst16++ = colour16;

	(*ppD3Dtex)->UnlockRect(0);

	return S_OK;
}



void CTools::ReleaseFont()
{
	if(pFont) pFont->Release();
	pFont = NULL;
}

void CTools::ReleaseTextures()
{
	
}

void CTools::BuildFont()
{
	HFONT hFont=CreateFont(14, //height
		0, 
		0, 
		0,
		FW_DONTCARE, 
		false, //itallic
		false, //underline
		false, //strikeout
		DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, 
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		TEXT("Courier New"));

	D3DXCreateFont(pD3Ddev, hFont, &pFont ); 

}

void CTools::DrawString(int x, int y, int r, int g, int b, int a, int flag, char *text, ...)
{
	if(!x || !y) return;
	if(pFont == NULL) return;
	if(!text) return;
	if(IsBadReadPtr((void*)pFont, 0x4)) return;

	va_list va_alist;
	char logbuf[256] = {0};
	va_start (va_alist, text);
	_vsnprintf (logbuf+strlen(logbuf), sizeof(logbuf) - strlen(logbuf), text, va_alist);
	va_end (va_alist);

	RECT font_rect;
	font_rect.top = y;
	font_rect.left= x;
	font_rect.right = x-0.001;
	font_rect.bottom = y-0.001;
	pFont->Begin();
	pFont->DrawText(logbuf, -1, &font_rect, flag|DT_NOCLIP, D3DCOLOR_ARGB(a, r, g, b));	
	pFont->End();
}

void CTools::DrawFilledQuad(int x, int y, int w, int h, int r, int g, int b, int a)
{
	D3DCOLOR rgba = D3DCOLOR_RGBA(r,g,b,a);

	D3DRECT box = {	x, y, x+w, y+h	};
	pD3Ddev->Clear(1, &box, D3DCLEAR_TARGET, rgba, 0, 0);
}

void CTools::DrawQuad(int x, int y, int w, int h, int r, int g, int b, int a, int thickness)
{
	D3DCOLOR rgba = D3DCOLOR_RGBA(r,g,b,a);

	D3DRECT oben =		{ x,	y,		x+w,		   y+thickness		};
	D3DRECT unten =		{ x,	y+h,	x+w,		   y+h+thickness	};
	D3DRECT links =		{ x,	y,		x+thickness,   y+h				};
	D3DRECT rechts =	{ x+w,	y,		x+w+thickness, y+h+1			};

	pD3Ddev->Clear(1, &oben, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &links, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &rechts, D3DCLEAR_TARGET, rgba, 0, 0);
	pD3Ddev->Clear(1, &unten, D3DCLEAR_TARGET, rgba, 0, 0);
}

void CTools::DrawPixel(int x, int y, int r, int g, int b, int a)
{
	D3DCOLOR rgba = D3DCOLOR_RGBA(r,g,b,a);

	D3DRECT pixel = { x, y, x+1, y+1	};
	pD3Ddev->Clear(1, &pixel, D3DCLEAR_TARGET, rgba, 0, 0);
}

void CTools::Crosshair(int type)
{
	D3DVIEWPORT8 oViewport;
	//pD3Ddev->GetViewport(&oViewport);
	oViewport.Width = cvars.w;
	oViewport.Height = cvars.h;
	int centerX = oViewport.Width/2;
	int centerY = oViewport.Height/2;
	
	if(type == 1)
	{
		DrawFilledQuad(centerX - 14, centerY, 9, 1,255,255,255,255);
		DrawFilledQuad(centerX +5, centerY, 9, 1,255,255,255,255);
		DrawFilledQuad(centerX, centerY - 14, 1, 9,255,255,255,255);
		DrawFilledQuad(centerX, centerY + 5, 1, 9,255,255,255,255);
		DrawFilledQuad(centerX, centerY , 1, 1,255,0,0,255);
	}
	else if(type == 2)
	{
		DrawFilledQuad(centerX - 14, centerY, 9, 2,255,255,255,255);
		DrawFilledQuad(centerX +5, centerY, 9, 2,255,255,255,255);
		DrawFilledQuad(centerX, centerY - 14, 2, 9,255,255,255,255);
		DrawFilledQuad(centerX, centerY + 5, 2, 9,255,255,255,255);
		DrawFilledQuad(centerX, centerY , 1, 2,255,0,0,255);
	}
	else if(type == 3)
	{
		DrawFilledQuad(centerX-25,centerY,50,1,255,255,255,255);
		DrawFilledQuad(centerX-5,centerY,10,1,255,0,0,255);
		DrawFilledQuad(centerX,centerY-25,1,50,255,255,255,255);
		DrawFilledQuad(centerX,centerY-5,1,10,255,0,0,255);
	}
	else if(type == 4)
	{
		DrawFilledQuad(centerX-25,centerY,50,2,255,255,255,255);
		DrawFilledQuad(centerX-5,centerY,10,2,255,0,0,255);
		DrawFilledQuad(centerX,centerY-25,2,50,255,255,255,255);
		DrawFilledQuad(centerX,centerY-5,2,10,255,0,0,255);
	}
	else if(type == 5)
	{
		DrawQuad(centerX,0,0,centerY*2,255,255,255,255,1);
		DrawQuad(0,centerY,centerX*2,0,255,255,255,255,1);
		DrawFilledQuad(centerX,centerY-5,1,10,255,0,0,255);
		DrawFilledQuad(centerX-5,centerY,10,1,255,0,0,255);	
	}
}
bool CTools::WorldToScreen(vector vPlayer, int &x, int &y)
{
	x = 0; y = 0;
	D3DXVECTOR3 vWorld(vPlayer.x,vPlayer.y,vPlayer.z);

	//pD3Ddev->GetViewport(&viewPort);
	viewPort.Width = cvars.w;
	viewPort.Height = cvars.h;
	viewPort.X = 0; viewPort.Y = 0;
	viewPort.MinZ = 0; viewPort.MaxZ = 1;
	pD3Ddev->GetTransform(D3DTS_VIEW, &view);
	pD3Ddev->GetTransform(D3DTS_PROJECTION, &projection);
	pD3Ddev->GetTransform(D3DTS_WORLD, &world);

	D3DXVECTOR3 vScreen;
	D3DXVec3Project(&vScreen, &vWorld, &viewPort, &projection, &view, &world);
	if(vScreen.z < 1)
	{
		x = (int)vScreen.x;
		y = (int)vScreen.y;

		return x?true:false;
	}

	return true;
}