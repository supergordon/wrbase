#include "sdk.h"

CMenu* pMenu = new CMenu("Gordon WR", 100, 100, 130, true, true);

CMenu::CMenu(char* szTitelName, int x, int y, int w, bool bOnHighlighting, bool bShowValue)
{
	iElements = 0;
	iSelected = 0;
	this->x = x;
	this->y = y;
	this->w = w;
	this->bOnHighlighting = bOnHighlighting;
	this->bShowValue = bShowValue;
	this->bMenuActive = false;
	bActive = true;
	strcpy(szTitel, szTitelName);
}

CMenu::~CMenu()
{
	iElements = 0;
	iSelected = 0;
}

void CMenu::AddEntry(char* szName, int* pValue, int iMin, int iMax, int iStep)
{
	strcpy(list[iElements].szName, szName);
	list[iElements].pValue = pValue;
	list[iElements].iMin = iMin;
	list[iElements].iMax = iMax;
	list[iElements].iStep = iStep;

	++iElements;
}

bool CMenu::KeyHandling()
{
	if(!bActive) return false;

	if(GetAsyncKeyState(VK_UP) & 1)
	{
		if(iSelected <= 0) iSelected = iElements-1;
		else iSelected--;
		return true;
	}
	if(GetAsyncKeyState(VK_DOWN) & 1)
	{
		if(iSelected >= (iElements-1)) iSelected = 0;
		else iSelected++;
		return true;
	}
	if(GetAsyncKeyState(VK_LEFT) & 1)
	{
		*list[iSelected].pValue -= list[iSelected].iStep;
		if(*list[iSelected].pValue < list[iSelected].iMin) *list[iSelected].pValue = list[iSelected].iMax;
		return true;
	}
	if(GetAsyncKeyState(VK_RIGHT) & 1)
	{
		*list[iSelected].pValue += list[iSelected].iStep;
		if(*list[iSelected].pValue > list[iSelected].iMax) *list[iSelected].pValue = list[iSelected].iMin;
		return true;
	}

	return false;
}


void CMenu::Draw()
{
	pTools->DrawFilledQuad(x+2, y+15, 133, 12*iElements+37, 0, 0, 0, 255);
	pTools->DrawQuad(x+2, y+15, 133, 12*iElements+37, 255, 255, 255, 255, 2);
	pTools->DrawFilledQuad(x+2, y+40, 133, 2, 255, 255, 255, 255);
	pTools->DrawString(x+133*0.5, y+22, 0, 255, 0, 255, DT_CENTER, "%s", szTitel);
	

	for(int i = 0; i < iElements; i++)
	{
		if(i != iSelected)
		{
			if(bOnHighlighting && *list[i].pValue)
			{
				pTools->DrawString(x+10, y+46+(12*i), 255, 255, 0, 255, DT_LEFT, "%s", list[i].szName);
				if(bShowValue && list[i].szName[0] != '-') pTools->DrawString(x+130, y+46+(12*i), 0, 255, 0, 255, DT_RIGHT, "%d", *list[i].pValue);
			}
			else
			{
				pTools->DrawString(x+10, y+46+(12*i), 255, 255, 255, 255, DT_LEFT, "%s", list[i].szName);
				if(bShowValue && list[i].szName[0] != '-') pTools->DrawString(x+130, y+46+(12*i), 255, 255, 255, 255, DT_RIGHT, "%d", *list[i].pValue);
			}
		}
		else
		{
			pTools->DrawString(x+10, y+46+(12*i), 0, 255, 0, 255, DT_LEFT, "%s", list[i].szName);
			if(bShowValue && list[i].szName[0] != '-') pTools->DrawString(x+130, y+46+(12*i), 255, 255, 0, 255, DT_RIGHT, "%d", *list[i].pValue);
		}
	}
}
