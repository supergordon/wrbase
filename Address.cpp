#include "sdk.h"


CAddress::CAddress()
{
	memset(&addy, 0, sizeof(addy));
	count = 0;
}

void CAddress::Add(DWORD adresse, int* var, BYTE* p, BYTE* b, int size)
{
	addy[count].address = adresse;
	addy[count].cvar = var;
	addy[count].size = size;
	memcpy(&addy[count].patch, p, size);
	memcpy(&addy[count].backup, b, size);
	count++;
}

void CAddress::AddNakedHook(DWORD adresse, int* var, void* function, BYTE* b)
{
	addy[count].address = adresse;
	addy[count].cvar = var;
	addy[count].size = 5;
	addy[count].hook = true;
	addy[count].function = function;
	memcpy(&addy[count].backup, b, 5);
	count++;
}

void CAddress::Restore()
{
	for(int i = 0; i < count; i++)
	{
		if(addy[i].cvar && *addy[i].cvar == 0 && addy[i].size)
		{
			BYTE buf = 0; memcpy(&buf, (void*)addy[i].address, 1);
			if(buf != addy[i].backup[0])
			{
				pTools->Patch(addy[i].address, addy[i].backup, addy[i].size);
			}
		}
	}
}

void CAddress::Apply()
{
	for(int i = 0; i < count; i++)
	{
		if(addy[i].cvar && *addy[i].cvar && addy[i].size)
		{
			BYTE buf = 0; memcpy(&buf, (void*)addy[i].address, 1);
			if((buf != addy[i].patch[0] && !addy[i].hook ) || (addy[i].hook && *(BYTE*)addy[i].address != 0xE9))
			{
				if(addy[i].hook)
				{
					pTools->DetourFunc((PBYTE)addy[i].address, (PBYTE)addy[i].function, 5);
					/*char keke[100]="";sprintf(keke, "%X %X %X", addy[i].address, addy[i].function, *(BYTE*)addy[i].address);
					MessageBox(0, keke, keke, 0);*/
				}
				else
					pTools->Patch(addy[i].address, addy[i].patch, addy[i].size);
			}
		}
	}
}