#ifndef _TOOLS_H_
#define _TOOLS_H_

#include "sdk.h"
struct vector;


class CTools
{
public:
	void LogText(char* szText, ...);
	void *DetourFunc(BYTE *src, const BYTE *dst, const int len);
	bool RetourFunc(BYTE *src, BYTE *restore, const int len);
	DWORD dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask);
	void Patch(DWORD dwAddress, BYTE* lala, int size);
	void ReleaseFont();
	void ReleaseTextures();
	void BuildFont();
	HRESULT GenerateTexture(LPDIRECT3DDEVICE8 pDevice,IDirect3DTexture8 **ppD3Dtex, DWORD colour32);
	void DrawString(int x, int y, int r, int g, int b, int a, int flag, char *text, ...);
	void DrawFilledQuad(int x, int y, int w, int h, int r, int g, int b, int a);
	void DrawQuad(int x, int y, int w, int h, int r, int g, int b, int a, int thickness);
	void DrawPixel(int x, int y, int r, int g, int b, int a);
	void Crosshair(int type);
	bool WorldToScreen(vector vPlayer, int &x, int &y);
}; extern CTools* pTools;

extern ID3DXFont* pFont;
extern D3DXMATRIX view, projection, world;
extern D3DVIEWPORT8 viewPort;

#endif